package mx.ms.eni.test.business.impl;

import org.springframework.stereotype.Service;

import mx.ms.eni.test.business.ITestService;

@Service
public class TestServiceImpl implements ITestService {

	public String getCadena() {
		String cadena = "Hola mundo";
		return cadena;
	}
}

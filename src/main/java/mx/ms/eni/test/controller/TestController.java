package mx.ms.eni.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.ms.eni.test.business.ITestService;
import mx.ms.eni.test.service.ITestPublicService;

@RestController
public class TestController implements ITestPublicService {

	@Autowired
	private ITestService testService;
	
	@Override
	@CrossOrigin(origins="*")
	@GetMapping(value="/", produces="application/json")
	public String getCadena() {
		return testService.getCadena();
	}
	
}
